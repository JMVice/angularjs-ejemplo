// ==================================================================================================================================
// ==================================================================================================================================
// ==================================================================================================================================
// Aquí va toda la lógica de AngularJS dentro de esta aplicación.

// Inicialización de AngularJS.
// Declaración de la variable que lleva la carga de la librería de Angular. En este caso se llama "myApp".
var myApp = angular.module("myApp", []);

// ==================================================================================================================================
// ==================================================================================================================================
// ==================================================================================================================================
// Funciones.


// Función controlador de ejemplo "Ejemplo de renderización de una lista con bucle.".
// Declaramos un nuevo controlador llamado "listaBucle". Este controlador es utilizado
// para el ejemplo "Ejemplo de renderización de una lista con bucle.".
myApp.controller("listaBucle", function listaBucle_controlador($scope) {
    // Utilizamos la variable llamada $scope para crear un arreglo llamado telefonos.
    // Para aprender más acerca de $scope, leer su documentación aquí: https://docs.angularjs.org/api/ng/type/$rootScope.Scope .
    $scope.telefonos = [
        {
            nombre: 'iPhone'
        },
        {
            nombre: 'Xiaomi'
        },
        {
            nombre: 'Nexus'
        },
        {
            nombre: 'Xiaomi'
        },
        {
            nombre: 'Samsung'
        }
    ];
});

// Funcion para el ejemplo de calculadora.
// Se creo un controlador llamado "calculadora". 
// El controlador da la lógica de la funcion "calcular".
// La funcion calcular se encarga de mostrar el pantalla el resultado de la operación
// aritmetica.
myApp.controller("calculadora", function calculadora_controller($scope) {
    $scope.calcular = function () {
        // Funcion que revisa la operación que el usuario desea hacer.
        let operacion = extraerOperadorSeleccionado();
        switch (operacion) {
            case "suma":
                // Establece en {{ resultado }} el resultado de numero_1 y numero_2 .
                // Y así con todas las demás opciones.
                $scope.resultado = $scope.numero_1 + $scope.numero_2;
                break;
            case "resta":
                $scope.resultado = $scope.numero_1 - $scope.numero_2;
                break;
            case "multiplicacion":
                $scope.resultado = $scope.numero_1 * $scope.numero_2;
                break;
            case "division":
                $scope.resultado = $scope.numero_1 / $scope.numero_2;
                break;
            default:
                $scope.resultado = $scope.numero_1 + $scope.numero_2;
                break;
        }
    }
});

// Extrae que tipo de operacion se va a realizar para el ejemplo: 
// "Ejemplo de calculadora.".
function extraerOperadorSeleccionado() {
    let operador = document.getElementById("operador");
    return operador.options[operador.selectedIndex].value;
}